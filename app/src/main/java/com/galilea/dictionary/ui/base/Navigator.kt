package com.galilea.dictionary.ui.base

import android.os.Bundle
import android.transition.Slide
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.TransitionChangeHandler
import com.galilea.dictionary.ui.search.SearchController

class Navigator(private val router: Router) {

    fun handleBack() = router.handleBack()

    fun home() {
        if (!router.hasRootController()) {
            setRoot()
        }
        openScreen(SearchController())
    }

    fun setRoot() = router.setRoot(RouterTransaction.with(SearchController()))

    fun saveState(outState: Bundle) = router.saveInstanceState(outState)
    fun restoreState(savedState: Bundle?) = router.restoreInstanceState(savedState ?: Bundle.EMPTY)

    fun openScreen(controller: Controller) {
        router.pushController(
            RouterTransaction
                .with(controller)
        )
    }
}