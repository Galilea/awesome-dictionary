package com.galilea.dictionary.ui.search

import com.galilea.dictionary.R
import com.galilea.dictionary.data.models.Meaning
import com.galilea.dictionary.ext.loadCircularPreview
import com.galilea.dictionary.ext.spannableMeaning
import com.galilea.dictionary.ui.base.view.BaseAdapter
import kotlinx.android.synthetic.main.item_meaning.view.*

class MeaningsAdapter: BaseAdapter<Meaning>() {

    override fun itemLayout(viewType: Int)
            = R.layout.item_meaning

    override fun ViewHolder.bind(item: Meaning, position: Int) {
        with(itemView) {
            preview_image_view.loadCircularPreview(item.previewUrl)
            meaning_text_view.text = spannableMeaning(context, item)
        }
    }

    override fun compareItems(oldItem: Meaning, newItem: Meaning)
            = oldItem.id == newItem.id
}