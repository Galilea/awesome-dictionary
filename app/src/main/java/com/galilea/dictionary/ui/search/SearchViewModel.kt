package com.galilea.dictionary.ui.search

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.galilea.dictionary.data.models.Word
import com.galilea.dictionary.data.models.WrappedResponse
import com.galilea.dictionary.interactors.search.SearchInteractor
import com.galilea.dictionary.ui.base.viewmodel.BaseViewModel
import org.koin.standalone.inject

class SearchViewModel: BaseViewModel() {

    private val interactor: SearchInteractor by inject()
    private val searchLiveData: MutableLiveData<WrappedResponse<List<Word>>> by livedata(null)

    private suspend fun searchAsync(query: String) =
        searchLiveData.apply {
            value = interactor.search(query)
        }

    fun search(lifecycleOwner: LifecycleOwner, query: String?, observer: Observer<WrappedResponse<List<Word>>>) =
        observe(lifecycleOwner, observer) { searchAsync(query ?: "") }

}