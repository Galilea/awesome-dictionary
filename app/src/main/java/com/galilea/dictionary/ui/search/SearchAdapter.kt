package com.galilea.dictionary.ui.search

import com.galilea.dictionary.R
import com.galilea.dictionary.data.models.Meaning
import com.galilea.dictionary.data.models.Word
import com.galilea.dictionary.ext.setItems
import com.galilea.dictionary.ext.switchVisibility
import com.galilea.dictionary.ui.base.view.BaseAdapter
import com.galilea.dictionary.ui.base.view.DefaultDecoration
import kotlinx.android.synthetic.main.item_search.view.*
import kotlinx.android.synthetic.main.item_word.view.*


class SearchAdapter : BaseAdapter<Word>() {

    var onMeaningClick: ((Meaning, Int) -> Unit)? = null

    override fun compareItems(oldItem: Word, newItem: Word) =
        oldItem.id == newItem.id

    override fun ViewHolder.bind(item: Word, position: Int) {
        with(itemView) {
            word_view.apply {
                text = item.text
                setOnClickListener {
                    itemView.meanings_list_view.switchVisibility()
                }
            }

            meanings_list_view.apply {
                addItemDecoration(DefaultDecoration(context))
                setItems(item.meanings ?: emptyList()) {
                    MeaningsAdapter().apply {
                        onItemClick = onMeaningClick
                    }
                }
            }
        }
    }

    override fun itemLayout(viewType: Int): Int = R.layout.item_search

}