package com.galilea.dictionary.ui.base.view

import androidx.appcompat.widget.SearchView

class SimpleSearchListener(val onChange: (String) -> Unit = {},
                           val onSubmit: (String) -> Unit = {}) : SearchView.OnQueryTextListener {

    constructor(onQuery: (String) -> Unit) : this(onQuery, onQuery)

    override fun onQueryTextSubmit(query: String?): Boolean {
        onSubmit(query ?: "")
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        onChange(newText ?: "")
        return true
    }

}