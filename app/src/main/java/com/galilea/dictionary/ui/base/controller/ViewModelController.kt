package com.galilea.dictionary.ui.base.controller

import android.content.ComponentCallbacks
import android.content.res.Configuration
import android.os.Bundle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import com.bluelinelabs.conductor.RestoreViewOnCreateController
import com.galilea.dictionary.ui.base.viewmodel.BaseViewModel
import org.koin.core.parameter.parametersOf
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import kotlin.reflect.KClass

abstract class ViewModelController(args: Bundle?) : RestoreViewOnCreateController(args), LifecycleOwner, KoinComponent, ComponentCallbacks {

    private val lifecycleOwner = ControllerLifecycleOwner(this)

    private val viewModelStore : ViewModelStore by inject()
    private val viewModelProvider: ViewModelProvider by inject { parametersOf(viewModelStore) }

    override fun getLifecycle() = lifecycleOwner.lifecycle

    fun <T: BaseViewModel> retrieve(vmClass: KClass<T>) = viewModelProvider.get(vmClass.java)

    override fun onLowMemory() {}
    override fun onConfigurationChanged(p0: Configuration?) {}

    override fun onDestroy() {
        super.onDestroy()
        viewModelStore.clear()
    }
}