package com.galilea.dictionary.ui.search

import android.text.TextUtils
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.lifecycle.Observer
import com.galilea.dictionary.R
import com.galilea.dictionary.ext.hideKeyboard
import com.galilea.dictionary.ext.setItems
import com.galilea.dictionary.ext.showView
import com.galilea.dictionary.ui.base.controller.BaseController
import com.galilea.dictionary.ui.base.view.SimpleSearchListener
import com.galilea.dictionary.ui.base.view.SimpleTextWatcher
import com.galilea.dictionary.ui.details.DetailsController
import kotlinx.android.synthetic.main.screen_search.view.*
import kotlin.reflect.KClass

class SearchController: BaseController<SearchViewModel>() {

    override fun layoutId(): Int = R.layout.screen_search
    override fun getViewModelClass(): KClass<SearchViewModel> = SearchViewModel::class

    private val queryListener = SimpleTextWatcher(
        afterChange = { newWord -> search(newWord) },
        onChange = { query ->
            view?.progress_bar?.showView(!TextUtils.isEmpty(query))
            performTransition(view as? ViewGroup, TextUtils.isEmpty(query))
        }
    )

    override fun initView(root: ViewGroup) {
        super.initView(root)
        root.search_view.addTextChangedListener(queryListener)
    }

    override fun destroyView(root: ViewGroup) {
        root.search_view.removeTextChangedListener(queryListener)
        super.destroyView(root)
    }

    private fun search(word: String) = viewModel.search(this, word, Observer { response ->
        handleWrappedResponse(response) {
            result -> view?.progress_bar?.showView(result.isNullOrEmpty())
            view?.results_list_view?.apply {
                setItemViewCacheSize(result.size)
                setItems(result) {
                    SearchAdapter().apply {
                        onMeaningClick = { meaning, _ ->
                            view?.search_view?.hideKeyboard(context)
                            openScreen(DetailsController.newInstance(meaning.id.toString()))
                        }
                    }
                }
            }
        }
    })

    private fun performTransition(root: ViewGroup?, isReverse: Boolean) {
        if (isReverse) {
            (root as? MotionLayout)?.transitionToStart()
        } else {
            (root as? MotionLayout)?.transitionToEnd()
        }
    }

}