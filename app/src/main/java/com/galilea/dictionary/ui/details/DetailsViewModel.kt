package com.galilea.dictionary.ui.details

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.galilea.dictionary.data.models.FullMeaningInfo
import com.galilea.dictionary.data.models.WrappedResponse
import com.galilea.dictionary.interactors.details.DetailsInteractor
import com.galilea.dictionary.ui.base.viewmodel.BaseViewModel
import org.koin.standalone.inject

class DetailsViewModel: BaseViewModel() {

    private val interactor: DetailsInteractor by inject()
    private val detailsLiveData: MutableLiveData<WrappedResponse<List<FullMeaningInfo>>> by livedata(null)

    private suspend fun getMeaningsAsync(ids: String) = detailsLiveData.apply {
        value = interactor.getDetails(ids, 100)
    }

    fun getMeanings(lifecycleOwner: LifecycleOwner, ids: String?, observer: Observer<WrappedResponse<List<FullMeaningInfo>>>) =
        observe(lifecycleOwner, observer) { getMeaningsAsync(ids ?: "") }

}