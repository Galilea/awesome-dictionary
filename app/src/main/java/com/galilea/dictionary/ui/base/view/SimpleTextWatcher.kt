package com.galilea.dictionary.ui.base.view

import android.text.Editable
import android.text.TextWatcher

open class SimpleTextWatcher(val afterChange: (String) -> Unit = {},
                             val beforeChange: (String) -> Unit = {},
                             val onChange: (String) -> Unit = {}) : TextWatcher {
    override fun afterTextChanged(s: Editable?) {
        afterChange(s.toString())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        beforeChange(s.toString())
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        onChange(s.toString())
    }
}