package com.galilea.dictionary.ui.details

import com.galilea.dictionary.R
import com.galilea.dictionary.data.models.TextWithSound
import com.galilea.dictionary.ui.base.view.BaseAdapter
import kotlinx.android.synthetic.main.item_text_and_speech.view.*

class ExamplesAdapter: BaseAdapter<TextWithSound>() {

    override fun itemLayout(viewType: Int) =
        R.layout.item_text_and_speech

    override fun ViewHolder.bind(item: TextWithSound, position: Int) {
        with(itemView) {
            text_view.text = item.text
            sound?.prepare(item.soundUrl)
        }
    }

    override fun compareItems(oldItem: TextWithSound, newItem: TextWithSound) =
            oldItem.text == newItem.text
}