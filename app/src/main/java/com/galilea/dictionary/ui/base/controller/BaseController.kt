package com.galilea.dictionary.ui.base.controller

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.NonNull
import androidx.lifecycle.Observer
import com.bluelinelabs.conductor.Controller
import com.galilea.dictionary.R
import com.galilea.dictionary.data.models.WrappedResponse
import com.galilea.dictionary.ui.base.Navigator
import com.galilea.dictionary.ui.base.viewmodel.BaseViewModel
import com.google.android.material.snackbar.Snackbar
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import retrofit2.HttpException
import java.io.IOException
import kotlin.reflect.KClass

abstract class BaseController<VM : BaseViewModel>(args: Bundle? = null) : ViewModelController(args) {

    constructor() : this(null)

    init {
        retainViewMode = RetainViewMode.RETAIN_DETACH
    }

    private val navigator: Navigator by inject { parametersOf(router) }

    lateinit var viewModel: VM

    open fun initView(root: ViewGroup){}
    open fun destroyView(root: ViewGroup){}

    @LayoutRes
    abstract fun layoutId(): Int

    abstract fun getViewModelClass(): KClass<VM>

    override fun onContextAvailable(context: Context) {
        super.onContextAvailable(context)
        viewModel = retrieve(getViewModelClass())
    }

    override fun onCreateView(@NonNull inflater: LayoutInflater, @NonNull container: ViewGroup, savedViewState: Bundle?): View {
        val root = inflater.inflate(layoutId(), container, false)
        initView(root as ViewGroup)
        return root
    }

    override fun onDestroyView(view: View) {
        destroyView(view as ViewGroup)
        super.onDestroyView(view)
    }

    protected fun openScreen(controller: Controller?) {
        controller?.let { navigator.openScreen(controller) }
    }

    protected fun observeLoadingState(progressBar: View) {
        viewModel.loadingState.observe(this@BaseController, Observer {
            progressBar.visibility = if(it) View.GONE else View.VISIBLE
        })
    }

    protected fun <T: Any> handleWrappedResponse(response: WrappedResponse<T>, observe: (T)-> Unit) {
        response.data?.let { observe(it) }
        response.error?.let { showSnackbar(it) }
    }

    protected fun showSnackbar(e: Throwable) {
        if(e is HttpException || e is IOException) {
            view?.let {
                Snackbar
                    .make(it, R.string.no_connection, Snackbar.LENGTH_LONG)
                    .setAction(R.string.close_app) { activity?.finish() }
                    .show()
            }
        } else {
            view?.let {
                Snackbar
                    .make(it, R.string.unknown_error, Snackbar.LENGTH_LONG)
                    .setAction(R.string.close_app) { activity?.finish() }
                    .show()
            }
        }

    }

}
