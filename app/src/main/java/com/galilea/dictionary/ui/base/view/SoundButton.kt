package com.galilea.dictionary.ui.base.view

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.util.AttributeSet
import android.widget.FrameLayout
import com.galilea.dictionary.R
import com.galilea.dictionary.ext.showView
import kotlinx.android.synthetic.main.view_sound.view.*
import kotlinx.coroutines.*
import java.io.IOException

class SoundButton @JvmOverloads constructor(
    context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        inflate(context, R.layout.view_sound, this)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setOnClickListener { playSound() }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        immediateStop()
        releasePlayer()
    }

    private var soundUrl: String? = null
    private var player: MediaPlayer? = null
    private var soundPlayingJob: Job? = null

    private var isPrepared: Boolean = false
    private var hasPendingStart: Boolean = false

    fun prepare(url: String?) {
        if(url.isNullOrEmpty()) {
            showView(false)
        } else {
            soundUrl = "https:$url"
            try {
                player = MediaPlayer().apply {
                    isLooping = false
                    setDataSource(soundUrl)
                    setAudioStreamType(AudioManager.STREAM_MUSIC)
                    prepareAsync()
                    setOnPreparedListener {
                        isPrepared = true
                        if (hasPendingStart) {
                            playAsync()
                        }
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun immediateStop() {
        soundPlayingJob?.isActive?.let {
            soundPlayingJob?.cancel()
        }
        soundPlayingJob = null
    }

    private fun releasePlayer() {
        player?.release()
        player = null
    }


    private fun playSound() {
        if(isPrepared) {
            playAsync()
        } else {
            hasPendingStart = true
        }
    }

    private fun lockButton(lock: Boolean) {
        this@SoundButton.apply {
            isEnabled = !lock
            isClickable = !lock

            progress.showView(lock)

            if(lock) {
                player?.let {
                    progress.max = it.duration
                }
            } else {
                hasPendingStart = false
            }
        }

    }

    private fun playAsync() {
        soundPlayingJob =
            CoroutineScope(Dispatchers.Main)
                .launch {
                    player?.start()
                    play.performClick()

                    lockButton(true)

                    player?.let {
                        while (it.isPlaying) {
                            progress.progress = it.currentPosition
                            delay(100L)
                        }
                    }

                    lockButton(false)
                }
    }

}