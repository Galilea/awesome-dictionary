package com.galilea.dictionary.ui.details

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.galilea.dictionary.R
import com.galilea.dictionary.ext.*
import com.galilea.dictionary.ui.base.controller.BaseController
import com.galilea.dictionary.ui.base.view.DefaultDecoration
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.screen_details.view.*
import kotlin.reflect.KClass

class DetailsController(args: Bundle?)  : BaseController<DetailsViewModel>(args) {

    override fun layoutId(): Int = R.layout.screen_details
    override fun getViewModelClass(): KClass<DetailsViewModel> = DetailsViewModel::class

    private val onExpandClick = View.OnClickListener {
        val expandable = view?.examples_list_view
        expandable?.switchVisibility()

        val textId = if(expandable?.isVisible == true)
            R.string.hide_examples
        else
            R.string.show_examples
        (it as? MaterialButton)?.setText(textId)
    }

    override fun initView(root: ViewGroup) {
        super.initView(root)
        loadInfo(args.getString(TAG_WORD))
    }

    private fun loadInfo(wordId: String?) =
        viewModel.getMeanings(this, wordId, Observer { response ->
            handleWrappedResponse(response) { result ->

                if(!result.isNullOrEmpty()){

                    val meaning = result[0]

                    view?.apply {

                        progress_bar?.showView(false)

                        header_text_view?.text = meaning.text

                        description_text_view?.text = spannableDescription(context, meaning)
                        definition_text_view?.text = spannableDefinition(context, meaning)

                        word_image_view?.loadImage(meaning.images?.firstOrNull()?.url)
                        sound?.prepare(meaning.soundUrl)

                        examples_button.apply {
                            showView(!meaning.examples.isNullOrEmpty())
                            setOnClickListener(onExpandClick)
                        }
                        examples_list_view?.apply {
                            setItems(meaning.examples ?: emptyList()) { ExamplesAdapter() }
                            addItemDecoration(DefaultDecoration(context))
                        }
                    }
                }
            }
        })

    companion object {
        private const val TAG_WORD = "TAG_WORD"

        fun newInstance(wordId: String?) =
            DetailsController(Bundle().apply {
                putString(TAG_WORD, wordId ?: "")
            })
    }


}