package com.galilea.dictionary.ui.base.controller

import android.view.View

import com.bluelinelabs.conductor.Controller

import androidx.annotation.NonNull
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry

internal class ControllerLifecycleOwner(controller: Controller) : Controller.LifecycleListener(), LifecycleOwner {

    private val lifecycleRegistry = LifecycleRegistry(this)

    init {
        controller.addLifecycleListener(object : Controller.LifecycleListener() {

            override fun preCreateView(@NonNull controller: Controller) {
                lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
            }

            override fun postAttach(@NonNull controller: Controller, @NonNull view: View) {
                lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START)
                lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
            }

            override fun preDetach(@NonNull controller: Controller, @NonNull view: View) {
                lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)
                lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
            }

            override fun postDestroyView(@NonNull controller: Controller) {
                lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            }

        })
    }

    @NonNull
    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }
}