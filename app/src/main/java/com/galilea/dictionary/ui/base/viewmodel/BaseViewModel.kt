package com.galilea.dictionary.ui.base.viewmodel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.standalone.KoinComponent

abstract class BaseViewModel : ViewModel(), KoinComponent {

    val loadingState: MutableLiveData<Boolean> by livedata(false)

    protected fun <T> observe(lifecycleOwner: LifecycleOwner, observer: Observer<T>,
                              getAsync: suspend () -> MutableLiveData<T>) {
        CoroutineScope(Dispatchers.Main)
                .launch {
                    loadingState.value = false
                    getAsync().observe(lifecycleOwner, observer)
                }
    }

    protected fun <T> livedata(newValue: T?) = lazy { MutableLiveData<T>()
            .apply {
                value = newValue
            }
    }
}