package com.galilea.dictionary.ui.base.view

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.galilea.dictionary.R
import com.galilea.dictionary.ext.inflate

abstract class BaseAdapter<T> : RecyclerView.Adapter<BaseAdapter.ViewHolder>() {

    var onItemClick: ((T, Int) -> Unit)? = null

    var items: List<T> = emptyList()
        set(value) {
            val diff = calculateDiff(field, value)
            field = value
            diff.dispatchUpdatesTo(this)
        }

    override fun getItemCount(): Int = items.size
    override fun getItemViewType(position: Int) = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(itemLayout(viewType))
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        val viewType = getItemViewType(position)
        with(holder) {
            itemView.apply {
                setTag(R.id.tag_item, item)
                setTag(R.id.tag_type, viewType)
                setOnClickListener { onItemClick?.invoke(item, viewType) }
            }
            bind(item, position)
        }
    }

    @LayoutRes
    abstract fun itemLayout(viewType: Int) : Int

    abstract fun ViewHolder.bind(item: T, position: Int)

    abstract fun compareItems(oldItem: T, newItem: T): Boolean

    open fun compareContents(oldItem: T, newItem: T): Boolean =
        oldItem == newItem

    private fun calculateDiff(oldItems: List<T>, newItems: List<T>): DiffUtil.DiffResult {
        return DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                compareItems(oldItems[oldItemPosition], newItems[newItemPosition])

            override fun getOldListSize() = oldItems.size

            override fun getNewListSize() = newItems.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                compareContents(oldItems[oldItemPosition], newItems[newItemPosition])

        })
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}