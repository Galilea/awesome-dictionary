package com.galilea.dictionary.di

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import com.bluelinelabs.conductor.Router
import com.galilea.dictionary.BuildConfig
import com.galilea.dictionary.DictionaryApp
import com.galilea.dictionary.data.rest.ApiService
import com.galilea.dictionary.data.rest.NetworkConnectionInterceptor
import com.galilea.dictionary.interactors.details.DetailsInteractor
import com.galilea.dictionary.interactors.search.SearchInteractor
import com.galilea.dictionary.ui.base.Navigator
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


val appModule = module {

    factory { Retrofit.Builder()
        .baseUrl(BuildConfig.API_ENDPOINT)
        .client(OkHttpClient.Builder()
            .callTimeout(10, TimeUnit.SECONDS)
            .cache((androidApplication() as? DictionaryApp)?.getCache())
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
//            .addInterceptor(object: NetworkConnectionInterceptor() {
//
//                override val isInternetAvailable: Boolean
//                    get() = (androidApplication() as? DictionaryApp)?.isInternetAvailable() ?: false
//
//                override fun onCacheUnavailable() {
//                }
//
//                override fun onInternetUnavailable() {
//                }
//
//            })
            .build())
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
        .create(ApiService::class.java) }

    factory { DetailsInteractor() }
    factory { SearchInteractor() }

}

val uiModule = module {

    single { (router: Router) -> Navigator(router) }

    single { ViewModelStore() }
    single { (viewModelStore: ViewModelStore) -> ViewModelProvider(viewModelStore, ViewModelProvider.AndroidViewModelFactory(androidApplication())) }
}

val modules = listOf(appModule, uiModule)