package com.galilea.dictionary.ext

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatTextView
import androidx.asynclayoutinflater.view.AsyncLayoutInflater
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.Slide
import androidx.transition.TransitionManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.galilea.dictionary.R
import com.galilea.dictionary.ui.base.view.BaseAdapter
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.RoundedCornerTreatment
import com.google.android.material.shape.ShapeAppearanceModel

fun View.switchVisibility() {
    showView(!isVisible)
}

fun View.showView(visible: Boolean) {
    visibility = if(visible) View.VISIBLE else View.GONE
}

fun View.hideKeyboard(context: Context) {
    val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager
    imm?.hideSoftInputFromWindow(windowToken, 0)
}

fun ViewGroup.inflate(layoutRes: Int): View =  LayoutInflater.from(context).inflate(layoutRes, this, false)

fun ViewGroup.inflateAsync(layoutRes: Int, parentViewGroup: ViewGroup?, callback: (View, Int, ViewGroup?) -> Unit) =
    AsyncLayoutInflater(context)
        .inflate(layoutRes, parentViewGroup) { view, resid, parent ->  callback(view, resid, parent) }

fun ViewGroup.addHeader(layoutRes: Int, initBlock: View.() -> Unit) =
    inflateAsync(layoutRes, null) { view, _, _ ->
        TransitionManager.beginDelayedTransition(this@addHeader, Slide(Gravity.TOP))
        addView(view, 0)
        view.initBlock()
    }

fun LinearLayout.addView(layoutRes: Int) = this.addView(View.inflate(context, layoutRes, null))

fun <T, A : BaseAdapter<T>> RecyclerView.setItems(items: List<T>, adapterFactory: () -> A) {
    if(adapter == null) {
        adapter = adapterFactory.invoke()
    }
    (adapter as? A)?.items = items
}

fun AppCompatTextView.drawableTint(@ColorInt color: Int) {
    if(Build.VERSION.SDK_INT < 23) {
        for(drawable in compoundDrawables) {
            drawable?.setColorFilter(color, PorterDuff.Mode.MULTIPLY)
        }
    }

}

fun ImageView.loadCircularPreview(url: String?) {
    load(url, RequestOptions()
            .placeholder(R.drawable.ic_image)
            .centerInside()
            .circleCrop()
        )
}

fun ImageView.loadImage(url: String?) {
    load(url, RequestOptions().placeholder(R.drawable.ic_image))
}

fun ImageView.load(url: String?, requestOptions: RequestOptions) {
    Glide.with(context)
        .load("https:$url")
        .apply(requestOptions)
        .into(this)
}

