package com.galilea.dictionary.ext

import android.content.Context
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.text.bold
import androidx.core.text.inSpans
import androidx.core.text.italic
import androidx.core.text.toSpannable
import com.galilea.dictionary.R
import com.galilea.dictionary.data.models.FullMeaningInfo
import com.galilea.dictionary.data.models.Meaning

fun spannableMeaning(context: Context?, meaning: Meaning): Spannable? {
    return SpannableStringBuilder()
        .inSpans(ForegroundColorSpan(ContextCompat.getColor(context!!, R.color.colorPrimary)))
        { italic { append(context.getText(nameResource(meaning.partOfSpeechCode))) } }
        .append(String.format(" [%s] \n", meaning.transcription))
        .inSpans(ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)))
        { bold { append(meaning.translation?.text) } }
        .toSpannable()
}

fun spannableDescription(context: Context?, meaning: FullMeaningInfo): Spannable? {
    return SpannableStringBuilder()
        .inSpans(ForegroundColorSpan(ContextCompat.getColor(context!!, R.color.colorPrimary)))
        { italic { append(context.getText(nameResource(meaning.partOfSpeechCode))) } }
        .append(String.format(" [%s] \n", meaning.transcription))
        .toSpannable()
}

fun spannableDefinition(context: Context?, meaning: FullMeaningInfo): Spannable? {
    return SpannableStringBuilder()
        .inSpans(ForegroundColorSpan(ContextCompat.getColor(context!!, R.color.colorPrimary)))
        { italic { String.format(" [%s] \n", meaning.definition?.text ?: "") } }
        .inSpans(ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorAccent)))
        { bold { append(meaning.translation?.text) } }
        .toSpannable()
}

@StringRes
fun nameResource(code: String?) = when(code) {
    "n" -> R.string.n
    "v" -> R.string.v
    "j" -> R.string.j
    "r" -> R.string.r
    "prp" -> R.string.prp
    "prn" -> R.string.prn
    "crd" -> R.string.crd
    "cjc" -> R.string.cjc
    "exc" -> R.string.exc
    "det" -> R.string.det
    "abb" -> R.string.abb
    "x" -> R.string.x
    "ord" -> R.string.ord
    "md" -> R.string.md
    "ph" -> R.string.ph
    "phi" -> R.string.phi
    else -> R.string.empty
}