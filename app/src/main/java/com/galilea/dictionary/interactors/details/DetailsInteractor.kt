package com.galilea.dictionary.interactors.details

import com.galilea.dictionary.data.models.FullMeaningInfo
import com.galilea.dictionary.data.models.WrappedResponse
import com.galilea.dictionary.interactors.base.BaseInteractor

class DetailsInteractor : BaseInteractor(){

    suspend fun getDetails(idsList: String, qual: Int?): WrappedResponse<List<FullMeaningInfo>> {
        return try {
            WrappedResponse(data = requestAsync { service.getMeaningsAsync(ids = idsList, quality = qual) }.await())
        } catch (e: Exception){
            WrappedResponse(error = e)
        }
    }
}