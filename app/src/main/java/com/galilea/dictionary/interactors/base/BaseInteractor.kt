package com.galilea.dictionary.interactors.base

import com.galilea.dictionary.data.rest.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

open class BaseInteractor : KoinComponent {

    val service: ApiService by inject()

    protected suspend fun <T> requestAsync(request: () -> T) = withContext(Dispatchers.IO) {
        request()
    }

}