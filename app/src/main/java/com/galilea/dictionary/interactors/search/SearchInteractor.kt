package com.galilea.dictionary.interactors.search

import com.galilea.dictionary.data.models.Word
import com.galilea.dictionary.data.models.WrappedResponse
import com.galilea.dictionary.interactors.base.BaseInteractor

class SearchInteractor : BaseInteractor(){

    suspend fun search(word: String): WrappedResponse<List<Word>> {
        return try {
            WrappedResponse(data = requestAsync { service.searchAsync(search = word) }.await())
        } catch (e: Exception){
            WrappedResponse(error = e)
        }
    }

}