package com.galilea.dictionary

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.galilea.dictionary.di.modules
import okhttp3.Cache
import org.koin.android.ext.android.startKoin
import java.io.File

class DictionaryApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, modules)
    }

    fun isInternetAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun getCache(): Cache {
        val cacheDir = File(cacheDir, "cache")
        return Cache(cacheDir, DISK_CACHE_SIZE)
    }

    companion object {
        const val DISK_CACHE_SIZE: Long = 10 * 1024 * 1024
    }

}