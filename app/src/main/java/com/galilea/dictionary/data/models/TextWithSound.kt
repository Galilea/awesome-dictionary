package com.galilea.dictionary.data.models

data class TextWithSound (
    val text: String?,
    val soundUrl: String?
)
