package com.galilea.dictionary.data.models

class WrappedResponse<T: Any>(
  val data: T? = null,
  val error: Throwable? = null
)