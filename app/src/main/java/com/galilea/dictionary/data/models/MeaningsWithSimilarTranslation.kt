package com.galilea.dictionary.data.models


data class MeaningsWithSimilarTranslation(
    val meaningId: Int?,
    val frequencyPercent: String?,
    val partOfSpeechAbbreviation: String?,
    val translation: Translation?
)
