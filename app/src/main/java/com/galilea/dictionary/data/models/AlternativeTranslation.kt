package com.galilea.dictionary.data.models


data class AlternativeTranslation (
    val text: String?,
    val translation: Translation?
)
