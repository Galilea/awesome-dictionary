package com.galilea.dictionary.data.models

data class FullMeaningInfo (
        val id: String?,
        val wordId: Int?,
        val difficultyLevel: Int?,
        val partOfSpeechCode: String?,
        val prefix: Any?,
        val text: String?,
        val soundUrl: String?,
        val transcription: String?,
        val properties: Properties?,
        val updatedAt: String?,
        val mnemonics: Any?,
        val translation: Translation?,
        val images: List<Image>?,
        val definition: TextWithSound?,
        val examples: List<TextWithSound>?,
        val meaningsWithSimilarTranslation: List<MeaningsWithSimilarTranslation>?,
        val alternativeTranslations: List<AlternativeTranslation>?
)
