package com.galilea.dictionary.data.models

data class Word(
    var id: Int?,
    var text: String?,
    var meanings: List<Meaning>?
) {
    fun commaSeparatedMeaningIds() =
            meanings?.map { it.id }?.joinToString()
}