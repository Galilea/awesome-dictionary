package com.galilea.dictionary.data.rest

import com.galilea.dictionary.data.models.FullMeaningInfo
import com.galilea.dictionary.data.models.Word
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("meanings")
    fun getMeaningsAsync(@Query("width") width: String? = null,
                         @Query("quality") quality: Int? = null,
                         @Query("ids") ids: String): Deferred<List<FullMeaningInfo>>

    @GET("words/search")
    fun searchAsync(@Query("pageSize") pageSize: Int? = null,
                    @Query("page") page: Int? = null,
                    @Query("search") search: String): Deferred<List<Word>>

}