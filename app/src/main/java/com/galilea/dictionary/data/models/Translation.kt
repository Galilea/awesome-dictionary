package com.galilea.dictionary.data.models


data class Translation (
    var text: String?,
    var note: String?
)