package com.galilea.dictionary.data.models


data class Meaning(
        var id: Int?,
        var partOfSpeechCode: String?,
        var translation: Translation?,
        var previewUrl: String?,
        var imageUrl: String?,
        var transcription: String?,
        var soundUrl: String?
)