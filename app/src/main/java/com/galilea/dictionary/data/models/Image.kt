package com.galilea.dictionary.data.models


data class Image (
    val url: String?
)
